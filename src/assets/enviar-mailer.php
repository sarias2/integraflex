<?php
header("Content-Type: text/html; charset=UTF-8");
// Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';

// These must be at the top of your script, not inside a function
//Load Composer's autoloader
// require 'vendor/autoload.php';


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

console.log($request);

$from_name = $request->name;
$from_country = $request->country;
$from_city = $request->city;
$from_company = $request->company;
$from_email = $request->email;
$from_phone = $request->phone;
$message = $request->message;
$headers = "";

$contact = "<p><strong>Name:</strong> $from_name</p>
            <p><strong>Pais:</strong> $from_country</p>
            <p><strong>Ciudad:</strong> $from_city</p>
            <p><strong>Empresa:</strong> $from_company</p>
            <p><strong>Email:</strong> $from_email</p>
            <p><strong>Tel:</strong> $from_phone</p>";
$content = "<p>$message</p>";
// $contact = "<p>Funciona</p>";
// $content = "<p>El email</p>";
$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {

    //Server settings
    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'gator3241.hostgator.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'sergio@wd-studios.com';                 // SMTP username
    $mail->Password = '$ergi0123';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('sergio@wd-studios.com', 'Mailer');
    $mail->addAddress('sergio@wd-studios.com', 'Sergio Arias');     // Add a recipient
    // $mail->addAddress('ellen@example.com');               // Name is optional

    $mail->addReplyTo('$from_email', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = '$website: Información de Contacto - $from_name';
    $mail->Body    = '$contact $content';
    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

?>
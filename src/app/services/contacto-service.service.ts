import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export interface IMessage {
  name?: string;
  country?: string;
  city?: string;
  company?: string;
  email?: string;
  phone?: string;
  message?: string;
}

@Injectable()
export class ContactoServiceService {

  // private emailUrl = '/assets/enviar.php';

  constructor(/*private http: HttpClient*/) { }

    // sendEmail(message: IMessage): Observable<IMessage> | any {
    //   return this.http.post<IMessage>(this.emailUrl, message).subscribe(
    //     res => {
    //       console.log('Email enviado exitosamente', res);
    //     },
    //     (err: HttpErrorResponse) => {
    //       console.log(err.error);
    //       console.log(err.name);
    //       console.log(err.message);
    //       console.log(err.status);
    //     }
    //   );
    // }


}

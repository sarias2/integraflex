import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ContactoServiceService, IMessage } from '../../services/contacto-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styles: []
})
export class ContactoComponent implements OnInit {
  title = 'Contacto INTEGRAFLEX';
  // message: IMessage = {};
  name?: '';
  country?: '';
  city?: '';
  company?: '';
  email?: '';
  phone?: '';
  message?: '';
  // constructor(private contactoService: ContactoServiceService) { }
  constructor() { }

  // sendEmail(message: IMessage) {
    //   this.contactoService.sendEmail(message).subscribe(res => {
      //     console.log('AppComponent Success', res);
      //     $('.formulario')[0].reset();
  //   }, error => {
    //     console.log('AppComponent Error', error, message);
  //   });
  // }


  enviar() {

    var data = {
      service_id: 'wd_sergio',
      template_id: 'template_LM8RzdCP',
      user_id: 'user_HnPeBjNB6j8hbr2TslwSt',
      template_params: {
        name: this.name,
        country: this.country,
        city: this.city,
        company: this.company,
        phone: this.phone,
        email: this.email,
        message: this.message
      }
    };
    $.ajax('https://api.emailjs.com/api/v1.0/email/send', {
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json'
    }).done(function() {
      $('#confirmacion').css('display', 'flex');
    }).fail(function(error) {
      $('#negacion').css('display', 'flex');
    });
  }
  ngOnInit() {
    $(document).ready(function() {
      $('#message').keyup(function() {
        if ($('#message').val().length === 0  || $('#email').val().length === 0 ) {
          $('#enviar').prop('disabled', true);
        } else if ($('#message').val().length !== 0  && $('#email').val().length !== 0) {
          $('#enviar').prop('disabled', false);
        }
      });
      // $('.enviar').click(function() {
      // });
      $('.confirmacion').click(function() {
        $('.confirmacion').css('display', 'none');
        $('.confirmacion').trigger('reset');
        $('#name').val('');
        $('#country').val('');
        $('#city').val('');
        $('#company').val('');
        $('#phone').val('');
        $('#email').val('');
        $('#message').val('');
      });
      $('#verAviso').click(function() {
        $('.aviso').removeClass('cerrado');
        console.log('Holi');
      });
      $('#cerrarAviso').click(function() {
        $('.aviso').addClass('cerrado');
        console.log('Chau');
      });
      $('#check').click(function() {
        if ($('#check').prop('checked')) {
          $('#enviar').prop('disabled', false);
        } else {
          $('#enviar').prop('disabled', true);
        }
        console.log($('#check').prop('checked'));
      });
    });
  }
}

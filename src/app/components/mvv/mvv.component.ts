import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-mvv',
  templateUrl: './mvv.component.html',
  styles: []
})
export class MvvComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function() {
      $('.externo-mision').click(function() {
        $('.mision').trigger('mouseover');
      });
      $('.externo-vision').click(function() {
        $('.vision').trigger('mouseover');
      });
      $('.externo-valores').click(function() {
        $('.valores').trigger('mouseover');
      });
    });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
// import { APP_ROUTING } from './app.routes';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { QuienesComponent } from './components/quienes/quienes.component';
import { MvvComponent } from './components/mvv/mvv.component';
import { AsesoriaComponent } from './components/asesoria/asesoria.component';
import { ProductosComponent } from './components/productos/productos.component';
import { UbicacionComponent } from './components/ubicacion/ubicacion.component';
import { ContactoComponent } from './components/contacto/contacto.component';

import { ContactoServiceService } from './services/contacto-service.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NavbarComponent,
    QuienesComponent,
    MvvComponent,
    AsesoriaComponent,
    ProductosComponent,
    UbicacionComponent,
    ContactoComponent,

  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBdpFTP-MOB2L5FP0YCBeORMgWrfa1Dj18'
    }),
    FormsModule,
    // APP_ROUTING,
    RouterModule,
    HttpClientModule

  ],
  providers: [ContactoServiceService,
  HttpModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
